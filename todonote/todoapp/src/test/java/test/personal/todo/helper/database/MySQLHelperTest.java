package test.personal.todo.helper.database;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import roboguice.RoboGuice;
import roboguice.activity.RoboActivity;
import roboguice.test.RobolectricRoboTestRunner;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.inject.AbstractModule;
import com.google.inject.util.Modules;
import com.personal.todo.helper.database.MySQLHelper;
import com.xtremelabs.robolectric.Robolectric;

@RunWith(RobolectricRoboTestRunner.class)
public class MySQLHelperTest {

	private DummyActivity activity;

	@Before
	public void setup() {
		RoboGuice
				.setBaseApplicationInjector(
						Robolectric.application,
						RoboGuice.DEFAULT_STAGE,
						Modules.override(
								RoboGuice
										.newDefaultRoboModule(Robolectric.application))
								.with(new ContextModule()));
		activity = new DummyActivity();
		activity.onCreate(null);
	}

	@Test
	public void test_sql_helper_successful_creation() {
		MySQLHelper instance = RoboGuice.getInjector(activity).getInstance(
				MySQLHelper.class);
		Assert.assertNotNull(instance);
	}

	@Test
	public void test_database_request() {
		MySQLHelper instance = RoboGuice.getInjector(activity).getInstance(
				MySQLHelper.class);
		Assert.assertNotNull(instance);
		SQLiteDatabase db = instance.getWritableDatabase();
		Assert.assertNotNull(db);
	}

	class ContextModule extends AbstractModule {
		@Override
		protected void configure() {
			requestStaticInjection(MySQLHelper.class);
		}
	}

	class DummyActivity extends RoboActivity {
		@Override
		protected void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
			LinearLayout linear = new LinearLayout(this);
			TextView view = new TextView(this);
			linear.addView(view);
			setContentView(view);
		}
	}
}
