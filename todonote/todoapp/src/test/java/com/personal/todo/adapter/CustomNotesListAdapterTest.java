package com.personal.todo.adapter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import roboguice.RoboGuice;
import roboguice.test.RobolectricRoboTestRunner;
import android.view.View;
import android.widget.TextView;

import com.personal.todo.R;
import com.personal.todo.model.Note;
import com.xtremelabs.robolectric.Robolectric;

@RunWith(RobolectricRoboTestRunner.class)
public class CustomNotesListAdapterTest {

	CustomNoteListAdapter instance ;
	
	@Before
	public void setUp()
	{
		instance = RoboGuice.getInjector(
				Robolectric.application).getInstance(
				CustomNoteListAdapter.class);
	}
	
	@Test
	public void should_inject_adapter_successfully() {
		
		Assert.assertNotNull(instance);
	}
	
	@Test
	public void get_view_element_for_a_note()
	{
		Note note = new Note();
		note.setName("note");
		note.setId(1);
		instance.add(note);
		View view = instance.getView(0, null, null);
		TextView findViewById = (TextView)view.findViewById(R.id.noteName);
		Assert.assertEquals("note", findViewById.getText().toString());
	}
}
