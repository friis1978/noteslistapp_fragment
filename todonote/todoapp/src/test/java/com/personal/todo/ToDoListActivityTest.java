package com.personal.todo;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.util.MockUtil;

import roboguice.RoboGuice;
import roboguice.test.RobolectricRoboTestRunner;
import android.widget.ArrayAdapter;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.google.inject.TypeLiteral;
import com.google.inject.util.Modules;
import com.personal.todo.adapter.CustomNoteListAdapter;
import com.personal.todo.dao.NotesDAO;
import com.personal.todo.dao.NotesDAOImpl;
import com.personal.todo.helper.database.MySQLHelper;
import com.personal.todo.model.Note;
import com.xtremelabs.robolectric.Robolectric;

@RunWith(RobolectricRoboTestRunner.class)
public class ToDoListActivityTest {

	private ToDoListActivity context;

	private NotesDAO mockDao = Mockito.mock(NotesDAOImpl.class);

	@Before
	public void setup() {
		RoboGuice
				.setBaseApplicationInjector(
						Robolectric.application,
						RoboGuice.DEFAULT_STAGE,
						Modules.override(
								RoboGuice
										.newDefaultRoboModule(Robolectric.application))
								.with(new MyTestModule()));

		context = new ToDoListActivity();
	}
	
	@Test
	public void test_view_element_present()
	{
		
	}

	@Test
	public void test_note_dao_mock_instance() {

		context.onCreate(null);
		NotesDAO mockNotes = RoboGuice.getInjector(Robolectric.application).getInstance(
				NotesDAO.class);
		Assert.assertNotNull(mockNotes);
		Assert.assertEquals(Boolean.TRUE,new MockUtil().isMock(mockNotes));
	}

	
	@Test
	public void test_menu_items_present()
	{
		
	}
	
	@After
	public void teardown() {
		RoboGuice.util.reset();
	}

	class MyTestModule extends AbstractModule {

		@Override
		protected void configure() {
			bind(MySQLHelper.class).in(Scopes.SINGLETON);
			bind(new TypeLiteral<ArrayAdapter<Note>>(){}).to(CustomNoteListAdapter.class);
			bind(NotesDAO.class).toInstance(mockDao);

		}

	}
}
