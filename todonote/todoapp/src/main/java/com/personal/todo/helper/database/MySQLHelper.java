package com.personal.todo.helper.database;

import roboguice.inject.ContextSingleton;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.inject.Inject;

@ContextSingleton
public class MySQLHelper extends SQLiteOpenHelper{
	
	protected static Context context;
	public static final String DEFAULT_DATABASE_NAME="notes.db";
	public static final int DATABASE_VERSION =1;
	
	public static final String 	TABLE_NOTES = "todo_notes";
	public static final String 	COLUMN_ID = "_id";
	public static final String 	COLUMN_NAME="name";
	public static final String 	COLUMN_DESCRIPTION = "description";
	public static final String 	COLUMN_CREATION_DATE = "date_created";
	public static final String 	COLUMN_MODIFICATION_DATE = "date_last_modified";
	
	private String dataBaseName;
	public static final String 	TABLE_NOTES_CREATE = "create table " + TABLE_NOTES + "(" + 
			COLUMN_ID +	" integer primary key autoincrement," + 
			COLUMN_NAME + " text not null," + 
			COLUMN_DESCRIPTION + " text not null,"+
			COLUMN_CREATION_DATE + " text not null,"+
			COLUMN_MODIFICATION_DATE + " text not null);";
	
	public static final String[] ALL_COLUMNS = { MySQLHelper.COLUMN_CREATION_DATE,
			MySQLHelper.COLUMN_DESCRIPTION, MySQLHelper.COLUMN_ID,
			MySQLHelper.COLUMN_MODIFICATION_DATE, MySQLHelper.COLUMN_NAME,
			MySQLHelper.COLUMN_CREATION_DATE };
	
	@Inject
	public MySQLHelper(Context context) {

		super(context, DEFAULT_DATABASE_NAME, null, DATABASE_VERSION);
		dataBaseName = DEFAULT_DATABASE_NAME;
	}
//	
//	public MySQLHelper(Context context,String dataBaseName) {
//		super(context, dataBaseName, null, DATABASE_VERSION);
//		this.dataBaseName = dataBaseName;
//	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		
		db.execSQL(TABLE_NOTES_CREATE);
		Log.w("Creating database", dataBaseName);
		
	}
 

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(MySQLHelper.class.getName(),
		        "Upgrading database from version " + oldVersion + " to "
		            + newVersion + ", which will destroy all old data");
		    db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTES);
		    onCreate(db);
	}
}
