package com.personal.todo.fragment;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.personal.todo.dao.NotesDAO;
import com.personal.todo.model.Note;


public class AsyncListLoader extends AsyncTaskLoader<List<Note>> {

	private List<Note> notesList = new ArrayList<Note>();
	
	private NotesDAO notesDAO;
	
	private String searchString = null;
	
	
	public AsyncListLoader(Context context) {
		super(context);

	}
	
	public AsyncListLoader(Context context, NotesDAO notesDao,
			String searchString) {
		super(context);
		this.notesDAO = notesDao;
		this.searchString = searchString;
	}

	@Override
	public List<Note> loadInBackground() {
		 
		if (null == getSearchString() || getSearchString().trim().length() == 0)
			notesList = notesDAO.getAllNotes();
		else
			notesList = notesDAO.searchNoteByName(getSearchString());
		
		return notesList;
	}
	

	@Override
	public void deliverResult(List<Note> data) {
		if(isStarted())
		{
			super.deliverResult(data);
		}
	}

	@Override
	protected void onStartLoading() {
		if(notesList.size() != 0)
			deliverResult(notesList);
		else
			forceLoad();
		
	}
	@Override
	protected void onStopLoading() {
		cancelLoad();
		
	}
	
	@Override
	public void onCanceled(List<Note> data) {
		data.clear();
	}
	@Override
	protected void onReset() {
		super.onReset();
			
		onStopLoading();
		notesList.clear();
	}

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}
}
