package com.personal.todo;

import android.app.SearchManager.OnDismissListener;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.github.rtyley.android.sherlock.roboguice.activity.RoboSherlockFragmentActivity;
import com.google.inject.Inject;
import com.personal.todo.fragment.FragmentListener;
import com.personal.todo.fragment.NoteEditFragment;
import com.personal.todo.fragment.NoteEditFragment.Type;
import com.personal.todo.fragment.NotesListFragment;
import com.personal.todo.fragment.NotesListFragment.NotesListener;
import com.personal.todo.helper.database.MySQLHelper;
import com.personal.todo.model.Note;

public class ToDoListActivity extends RoboSherlockFragmentActivity implements NotesListener,FragmentListener, OnDismissListener {

	@Inject
	NotesListFragment notesListFragment;
	
	@Inject 
	NoteEditFragment createNoteFragment;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.empty_activity);
		if(findViewById(R.id.content) != null)
		{
			if(savedInstanceState != null)
				return;
			FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
			transaction.add(R.id.content, notesListFragment);

			transaction.commit();
		}
	}

	@Override
	public Note addNote() {
		Bundle bundle = createNoteFragment.getArguments();
		if(null == bundle)
			bundle = new Bundle();
		bundle.putString(NoteEditFragment.OP_TYPE,Type.CREATE.toString());
		createNoteFragment.setArguments(bundle);
		FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
		beginTransaction.replace(R.id.content, createNoteFragment);
		beginTransaction.addToBackStack(null);
		beginTransaction.commit();
		
		
		return null;	
	}

	@Override
	public Note editNote(String noteID) {
		
		Bundle bundle = createNoteFragment.getArguments();
		if(null == bundle)
			bundle = new Bundle();
		bundle.clear();
		bundle.putString(NoteEditFragment.OP_TYPE,Type.EDIT.toString());
		bundle.putString(MySQLHelper.COLUMN_ID,noteID );
		createNoteFragment.setArguments(bundle);
		
		FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
		beginTransaction.replace(R.id.content,createNoteFragment );
		beginTransaction.addToBackStack(null);
		beginTransaction.commit();
		return null;
	}

	@Override
	public boolean deleteNote(String noteID) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onFragmentComplete(Bundle extraInfo) {
		getSupportFragmentManager().popBackStack();
	}

	@Override
	public void onDismiss() {
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(R.id.content, notesListFragment);

		transaction.commit();
		
	}
}
