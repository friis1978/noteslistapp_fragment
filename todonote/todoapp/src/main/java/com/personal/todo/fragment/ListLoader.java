package com.personal.todo.fragment;

import java.util.List;

import roboguice.inject.ContextSingleton;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.widget.ArrayAdapter;

import com.google.inject.Inject;
import com.personal.todo.dao.NotesDAO;
import com.personal.todo.model.Note;

@ContextSingleton
public class ListLoader implements LoaderCallbacks<List<Note>> {

	public static final String SEARCH_KEY = "searchKey";
	
	@Inject
	ArrayAdapter<Note> mAdapter;
	
	
	AsyncListLoader listLoader;
	
	@Inject
	NotesDAO notesDao;

	private Context context;
	@Inject
	public ListLoader(Context context) {
		this.context = context;
	}
	

	@Override
	public Loader<List<Note>> onCreateLoader(int paramInt, Bundle params) {
		String searchString = null;
		if (null != params) {
			searchString = params.getString(ListLoader.SEARCH_KEY);
		}
		
		
		return new AsyncListLoader(context,notesDao,searchString);
	}

	@Override
	public void onLoadFinished(Loader<List<Note>> paramLoader, List<Note> paramD) {
		mAdapter.clear();
		mAdapter.addAll(paramD);
		mAdapter.sort(new Note.NameComparator());
		mAdapter.notifyDataSetChanged();
		
	}

	@Override
	public void onLoaderReset(Loader<List<Note>> paramLoader) {
		mAdapter.clear();
	}

	
}
