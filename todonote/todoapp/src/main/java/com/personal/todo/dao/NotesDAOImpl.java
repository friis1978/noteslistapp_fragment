package com.personal.todo.dao;

import java.util.ArrayList;
import java.util.List;

import roboguice.inject.ContextSingleton;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.inject.Inject;
import com.personal.todo.helper.database.MySQLHelper;
import com.personal.todo.model.Note;

@ContextSingleton
public class NotesDAOImpl implements NotesDAO {

	private SQLiteDatabase database;
	@Inject
	private MySQLHelper helper;

	public NotesDAOImpl() {

	}

	//
	// public NotesDAOImpl(Context context) {
	// //helper = new MySQLHelper(context);
	// }

	@Override
	public void openWritable() throws SQLException {
		database = helper.getWritableDatabase();
	}

	@Override
	public void openReadable() throws SQLException {
		database = helper.getReadableDatabase();
	}

	@Override
	public void close() {
		helper.close();
	}

	@Override
	public Note editNote(Note note) {
		openWritable();
		ContentValues values = new ContentValues();
		values.put(MySQLHelper.COLUMN_ID, note.getId());
		values.put(MySQLHelper.COLUMN_NAME, note.getName());
		values.put(MySQLHelper.COLUMN_DESCRIPTION, note.getDescription());
		values.put(MySQLHelper.COLUMN_MODIFICATION_DATE,
				note.getModifiedDateStr());
		int update = database.update(MySQLHelper.TABLE_NOTES, values,
				MySQLHelper.COLUMN_ID + "=" + note.getId(), null);
		Log.d(NotesDAOImpl.class.toString(), "number of rows updated:" + update);
		close();
		return note;
	}

	@Override
	public List<Note> getAllNotes() {
		openReadable();
		List<Note> notesList = new ArrayList<Note>();
		Cursor cursor = database.query(MySQLHelper.TABLE_NOTES,
				MySQLHelper.ALL_COLUMNS, null, null, null, null, null);
		
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			notesList.add(convertToNote(cursor));
			cursor.moveToNext();
		}
		close();
		
		return notesList;
	}

	private Note convertToNote(Cursor cursor) {
		Note note = new Note();
		note.setId(cursor.getInt(cursor.getColumnIndex(MySQLHelper.COLUMN_ID)));
		note.setDateCreatedFromStr(cursor.getString(cursor
				.getColumnIndex(MySQLHelper.COLUMN_CREATION_DATE)));
		note.setDateLastModifiedFromStr(cursor.getString(cursor
				.getColumnIndex(MySQLHelper.COLUMN_MODIFICATION_DATE)));
		note.setDescription(cursor.getString(cursor
				.getColumnIndex(MySQLHelper.COLUMN_DESCRIPTION)));
		note.setName(cursor.getString(cursor
				.getColumnIndex(MySQLHelper.COLUMN_NAME)));

		return note;
	}

	@Override
	public Note addNote(Note note) {
		openWritable();
		ContentValues values = new ContentValues();
		values.put(MySQLHelper.COLUMN_NAME, note.getName());
		values.put(MySQLHelper.COLUMN_DESCRIPTION, note.getDescription());
		values.put(MySQLHelper.COLUMN_CREATION_DATE, note.getCreationDateStr());
		values.put(MySQLHelper.COLUMN_MODIFICATION_DATE,
				note.getModifiedDateStr());
		long insertId = database.insert(MySQLHelper.TABLE_NOTES, null, values);
		close();
		return getNoteByRowID(insertId);
	}

	private Note getNoteByRowID(long rowID) {
		openReadable();
		Cursor cursor = database.query(MySQLHelper.TABLE_NOTES,
				MySQLHelper.ALL_COLUMNS, "_rowid_ = " + rowID, null, null,
				null, null);
		cursor.moveToFirst();
		Note noteByID = convertToNote(cursor);
		close();
		return noteByID;
	}

	@Override
	public boolean removeNoteById(int noteID) {
		openWritable();
		if (noteID < 0)
			return false;
		int delete = database.delete(MySQLHelper.TABLE_NOTES,
				MySQLHelper.COLUMN_ID + "=" + noteID, null);
		close();
		if (delete == 0)
			return false;
		return true;
	}

	@Override
	public Note editNote(int noteID, Note note) {
		note.setId(noteID);
		return editNote(note);
	}

	@Override
	public Note getNoteByID(int noteID) {
		openReadable();
		Cursor cursor = database.query(MySQLHelper.TABLE_NOTES,
				MySQLHelper.ALL_COLUMNS,
				MySQLHelper.COLUMN_ID + " = " + noteID, null, null, null, null);
		cursor.moveToFirst();
		Note noteByID = convertToNote(cursor);
		close();
		return noteByID;
	}

	@Override
	public List<Note> searchNoteByName(String searchString) {
		return getSearchResult(MySQLHelper.COLUMN_NAME, searchString);
	}

	@Override
	public List<Note> searchNoteByDescription(String searchString) {

		return getSearchResult(MySQLHelper.COLUMN_DESCRIPTION, searchString);

	}

	private List<Note> getSearchResult(String columnName, String searchString) {
		List<Note> notesResult = new ArrayList<Note>();
		if (null == searchString || searchString.trim().length() == 0)
			return notesResult;
		openReadable();
		Cursor result = database.query(MySQLHelper.TABLE_NOTES,
				MySQLHelper.ALL_COLUMNS, columnName + " like '%" + searchString
						+ "%'", null, null, null, null, null);
		result.moveToFirst();
		while (!result.isAfterLast()) {
			notesResult.add(convertToNote(result));
			result.moveToNext();
		}
		close();
		return notesResult;
	}
}
