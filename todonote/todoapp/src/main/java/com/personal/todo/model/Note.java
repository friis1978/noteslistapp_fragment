package com.personal.todo.model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

import com.personal.todo.helper.database.ServiceConstantHelper;

public class Note implements Serializable,Comparable<Note>{

	public static class NameComparator implements Comparator<Note> {

		@Override
		public int compare(Note lhs, Note rhs) {
			if(null == lhs || null == rhs)
				return -1;
			return lhs.getName().compareToIgnoreCase(rhs.getName());
		}

		
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer _id;

	private String name;

	private String description;

	private Date dateCreated;

	private Date dateLastModified;

	SimpleDateFormat format = new SimpleDateFormat(
			ServiceConstantHelper.DATE_PATTERN);

	public Integer getId() {
		return _id;
	}

	public void setId(Integer id) {
		this._id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateLastModified() {
		return dateLastModified;
	}

	public void setDateLastModified(Date dateLastModified) {
		this.dateLastModified = dateLastModified;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreationDateStr() {
		return format.format(dateCreated);
	}

	public String getModifiedDateStr() {
		return format.format(dateLastModified);
	}

	public void setDateCreatedFromStr(String dateCreation) {
		try {
			this.dateCreated = format.parse(dateCreation);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void setDateLastModifiedFromStr(String string) {
		try {
			this.dateLastModified = format.parse(string);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_id == null) ? 0 : _id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Note other = (Note) obj;
		if (_id == null) {
			if (other._id != null)
				return false;
		} else if (!_id.equals(other._id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Note [id=" + _id + ", name=" + name + ", description="
				+ description + ", dateCreated=" + dateCreated
				+ ", dateLastModified=" + dateLastModified + ", format="
				+ format + "]";
	}

	@Override
	public int compareTo(Note another) {
		
		if(null == another)
			return -1;
		
		return this.getName().compareToIgnoreCase(another.getName());
	}
}
