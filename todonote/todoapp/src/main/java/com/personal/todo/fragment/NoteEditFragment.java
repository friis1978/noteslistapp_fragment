package com.personal.todo.fragment;

import java.util.Calendar;

import roboguice.inject.ContextSingleton;
import roboguice.inject.InjectView;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rtyley.android.sherlock.roboguice.fragment.RoboSherlockFragment;
import com.google.inject.Inject;
import com.personal.todo.R;
import com.personal.todo.dao.NotesDAO;
import com.personal.todo.helper.database.MySQLHelper;
import com.personal.todo.model.Note;

@ContextSingleton
public class NoteEditFragment extends RoboSherlockFragment implements OnClickListener
{
	@InjectView(R.id.createNote)
	Button create;
	@InjectView(R.id.cancelCreate)
	Button cancel;
	@InjectView(R.id.name)
	TextView name;
	@InjectView(R.id.description)
	TextView description;
	
	@Inject
	NotesDAO notesDao;
	
	public static final String OP_TYPE = "opType";
	public enum Type
	{
		CREATE,EDIT
	}
	
	private String notesID;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		return inflater.inflate(R.layout.activity_create_event, container,false);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		
		create.setOnClickListener(this);
		cancel.setOnClickListener(this);
	}
	
	@SuppressWarnings("unused")
	@Override
	public void onAttach(Activity activity) {

		super.onAttach(activity);
		try {
			FragmentListener listener = (FragmentListener) getActivity();
		} catch (ClassCastException e) {
			throw new IllegalStateException(
					"Fragment Listener should be implemented by the activity");
		}
	}
	@Override
	public void onResume() {
		
		super.onResume();
		String type = getArguments().getString(OP_TYPE);
		if(null != type && type.equals(Type.EDIT.toString()))
			notesID = getArguments().getString(MySQLHelper.COLUMN_ID);
		else
			notesID = null;
		
		if(null != notesID)
		{
			Note note = notesDao.getNoteByID(Integer.valueOf(notesID));
			name.setText(note.getName());
			description.setText(note.getDescription());
			create.setText("Edit Note");
		}
		else
		{
			name.setText("");
			description.setText("");
		}
	}
	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.createNote)
		{
			if(((Button)v).getText() != "Edit Note")
			{
				Note note = new Note();
				note.setName(name.getText().toString());
				note.setDescription(description.getText().toString());
				note.setDateCreated(Calendar.getInstance().getTime());
				note.setDateLastModified(Calendar.getInstance().getTime());
				notesDao.addNote(note);
				Toast.makeText(getActivity(), "Note Added Successfully",
						Toast.LENGTH_LONG).show();
				((FragmentListener)getActivity()).onFragmentComplete(new Bundle());
			}
			else
			{
				Note note = new Note();
				note.setId(Integer.valueOf(notesID));
				note.setName(name.getText().toString());
				note.setDescription(description.getText().toString());
				note.setDateCreated(Calendar.getInstance().getTime());
				note.setDateLastModified(Calendar.getInstance().getTime());
				notesDao.editNote(note);
				Toast.makeText(getActivity(), "Note Edited Successfully",
						Toast.LENGTH_LONG).show();
				((FragmentListener)getActivity()).onFragmentComplete(new Bundle());
			}
			
			
		}
		if(v.getId() == R.id.cancelCreate)
		{
			((FragmentListener)getActivity()).onFragmentComplete(new Bundle());
		}
		
	}
	
	
	
}
