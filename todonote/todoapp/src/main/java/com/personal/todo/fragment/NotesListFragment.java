package com.personal.todo.fragment;

import roboguice.inject.ContextSingleton;
import roboguice.inject.InjectView;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;

import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.ActionMode.Callback;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnActionExpandListener;
import com.github.rtyley.android.sherlock.roboguice.activity.RoboSherlockFragmentActivity;
import com.github.rtyley.android.sherlock.roboguice.fragment.RoboSherlockListFragment;
import com.google.inject.Inject;
import com.personal.todo.R;
import com.personal.todo.dao.NotesDAO;
import com.personal.todo.model.Note;

@ContextSingleton
public class NotesListFragment extends RoboSherlockListFragment implements OnQueryTextListener{

	@InjectView(android.R.id.list)
	protected ListView listView;

	@Inject
	NotesDAO notesDao;

	@Inject
	ArrayAdapter<Note> notesAdapter;
	
	@Inject
	ListLoader notesListLoader;
	
	
	public interface NotesListener {

		public Note addNote();
		
		public Note editNote(String noteID);
		
		public boolean deleteNote(String noteID);
		
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setHasOptionsMenu(Boolean.TRUE);
		getLoaderManager().initLoader(0, new Bundle(), notesListLoader);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			@SuppressWarnings("unused")
			NotesListener listener = (NotesListener) activity;

		} catch (ClassCastException e) {
			Log.e(NotesListFragment.class.toString(),
					"Activity must implement NotesListener");
			throw new IllegalStateException(
					"Activity must implement NotesListener");
		}
	}

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
		
		switch (itemId) {
		case R.id.menuItemCreate:
			((NotesListener) getActivity()).addNote();
			return true;
		case android.R.id.home :
			notesAdapter.clear();
			notesAdapter.addAll(notesDao.getAllNotes());
			notesAdapter.sort(new Note.NameComparator());
			notesAdapter.notifyDataSetChanged();
			return true;
		default:
			break;
		}

		return false;
	}

	
	@Override
	public void onResume() {
		
		super.onResume();
		Log.e(NotesListFragment.class.toString(), "onResume");
		getLoaderManager().restartLoader(0, new Bundle(), notesListLoader);
		
	}
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		String notesID = (String)v.getTag(R.string.notesID);
		Callback cb = new ActionModeCallback(notesID);
		RoboSherlockFragmentActivity fm = ((RoboSherlockFragmentActivity) getActivity());
		fm.startActionMode(cb);

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.activity_to_do_list, menu);
	
		SearchView searchWidget = new SearchView(getActivity());
		MenuItem item = menu.findItem(R.id.searchNotes);		
		searchWidget.setSubmitButtonEnabled(Boolean.FALSE);	
		searchWidget.setOnQueryTextListener(this);
		item.setActionView(searchWidget);	
		item.setOnActionExpandListener(new OnActionExpandListener() {
			
			@Override
			public boolean onMenuItemActionExpand(MenuItem paramMenuItem) {
				return true;
			}
			
			@Override
			public boolean onMenuItemActionCollapse(MenuItem paramMenuItem) {
				NotesListFragment.this.onResume();
				return true;
			}
		});
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		Log.e(NotesListFragment.class.toString(), "OnCreateView");
		return inflater.inflate(R.layout.activity_to_do_list, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {

		super.onViewCreated(view, savedInstanceState);
		Log.e(NotesListFragment.class.toString(), "OnViewCreated");
	
		setListAdapter(notesAdapter);
		
		// registerForContextMenu(listView);
	}

	public void deleteNoteByID(int notesID) {
		Note noteByID = notesDao.getNoteByID(notesID);
		if (null != noteByID) {
			if (notesDao.removeNoteById(notesID)) {
				getLoaderManager().restartLoader(0, new Bundle(), notesListLoader);
			}
		}
	}

	private void editNote(int noteID) {
		((NotesListener)getActivity()).editNote(String.valueOf(noteID));
	}
	
	class ActionModeCallback implements Callback {

		private String notesID;

		public ActionModeCallback(String notesID) {
			this.notesID = notesID;
		}

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			menu.clear();
			mode.getMenuInflater().inflate(R.menu.menu_list_options, menu);
			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			int id = item.getItemId();
			
			switch (id) {
			case R.id.editNote:
				NotesListFragment.this.editNote(Integer.valueOf(notesID));
				mode.finish();
				return true;
			case R.id.deleteNote:
				NotesListFragment.this.deleteNoteByID(Integer.valueOf(notesID));
				mode.finish();
				return true;
			}
			mode.finish();
			return false;
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			notesID = null;
		}
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		handleSearch(query);
		return true;
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		handleSearch(newText);
		return true;
	}
	
	private void handleSearch(String text)
	{
		notesAdapter.clear();
		if(text.trim().length() != 0)
		{
			 Bundle b = new Bundle();
			 b.putString(ListLoader.SEARCH_KEY, text);
			getLoaderManager().restartLoader(0,b, notesListLoader);
		}
		
	}
}
