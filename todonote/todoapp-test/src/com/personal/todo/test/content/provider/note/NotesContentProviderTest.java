package com.personal.todo.test.content.provider.note;

import java.util.Calendar;

import android.annotation.TargetApi;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.test.ProviderTestCase2;
import android.test.mock.MockContentResolver;

import com.personal.todo.content.provider.note.NotesContentProvider;
import com.personal.todo.content.provider.note.NotesProvider;
import com.personal.todo.helper.database.MySQLHelper;

@TargetApi(16)
public class NotesContentProviderTest extends
		ProviderTestCase2<NotesContentProvider> {

	private MockContentResolver mResolver;

	public NotesContentProviderTest() {
		super(NotesContentProvider.class, NotesProvider.AUTHORITY);

	}

	protected void setUp() throws Exception {

		super.setUp();
		mResolver = getMockContentResolver();
	}

	public void testQueryData() {
		// Test that there is currently no data in the database
		Cursor query = mResolver.query(NotesProvider.Note.CONTENT_URI, null,
				null, null, null);
		assertNotNull(query);
		assertEquals(0, query.getCount());
		addData();
		query = mResolver.query(NotesProvider.Note.CONTENT_URI, null, null,
				null, null);
		assertNotNull(query);
		assertEquals(1, query.getCount());

	}

	public void testQueryDataBYNoteID() {
		// Test that there is currently no data in the database
		Cursor query = mResolver.query(NotesProvider.Note.CONTENT_URI, null,
				null, null, null);
		assertNotNull(query);
		assertEquals(0, query.getCount());
		// insertData();
		Uri addData = addData();
		String insertID = addData.getPathSegments().get(1);
		Uri notesIDUrl = ContentUris.withAppendedId(
				NotesProvider.Note.CONTENT_ITEM_URL, Long.valueOf(insertID));
		query = mResolver.query(notesIDUrl, null, null, null, null);
		assertNotNull(query);
		assertEquals(1, query.getCount());

	}

	public void test_query_data_invalid_uri() {
		String fakeUrl = "content://randomURL";
		Cursor query = mResolver.query(Uri.parse(fakeUrl), null, null, null,
				null);
		assertNull(query);
	}

	public void test_query_add_data_invalid_uri() {
		String fakeUrl = "content://randomURL";
		ContentValues values = getContentValues();

		try {
			mResolver.insert(Uri.parse(fakeUrl), values);
			fail("Should not reach here");
		} catch (IllegalArgumentException e) {

		}

	}

	public void testDeleteNoteByID() {
		Uri addData = addData();
		int delete = mResolver.delete(addData, null, null);
		assertEquals(1, delete);
	}

	public void testDeleteNoteByWhereClauseFailed() {
		addData();
		int delete = mResolver.delete(NotesProvider.Note.CONTENT_URI,
				MySQLHelper.COLUMN_NAME + "=?", new String[] { "string" });
		assertEquals(0, delete);
	}

	public void testDeleteNoteByWhereClauseSuccess() {
		addData();
		int delete = mResolver.delete(NotesProvider.Note.CONTENT_URI,
				MySQLHelper.COLUMN_NAME + "=?", new String[] { "testName" });
		assertEquals(1, delete);
	}

	public void testDeleteAllNotes() {

		addData();
		addData();

		int delete = mResolver.delete(NotesProvider.Note.CONTENT_URI, null,
				null);
		assertEquals(2, delete);
	}

	public void testUpdateNote() {
		ContentValues contentValues = getContentValues();
		String value = "modified";
		contentValues.put(MySQLHelper.COLUMN_NAME, value);
		
		Uri add = addData();
		
		int update = mResolver.update(add, contentValues, null, null);
		assertEquals(1, update);
		Cursor query = mResolver.query(add, null, null, null, null);
		int columnIndex = query.getColumnIndex(MySQLHelper.COLUMN_NAME);

		String string = query.getString(columnIndex);
		assertEquals(value, string);
	}

	public void testInsertData() {
		Uri insert = addData();
		assertNotNull(insert);
		Cursor query = mResolver.query(insert, null, null, null, null);
		int columnIndex = query.getColumnIndex(MySQLHelper.COLUMN_NAME);

		String string = query.getString(columnIndex);
		assertNotNull(string);

	}

	private Uri addData() {
		ContentValues values = getContentValues();
		Uri insert = mResolver.insert(NotesProvider.Note.CONTENT_URI, values);
		return insert;
	}

	private ContentValues getContentValues() {
		ContentValues values = new ContentValues();
		values.put(MySQLHelper.COLUMN_NAME, "testName");
		values.put(MySQLHelper.COLUMN_DESCRIPTION, "testDesc");
		values.put(MySQLHelper.COLUMN_CREATION_DATE, Calendar.getInstance()
				.getTime().toString());
		values.put(MySQLHelper.COLUMN_MODIFICATION_DATE, Calendar.getInstance()
				.getTime().toString());
		return values;
	}

	protected void tearDown() throws Exception {
		super.tearDown();

	}

	public void testGetTypeUri() {
		String type = mResolver.getType(NotesProvider.Note.CONTENT_URI);
		assertEquals(NotesProvider.Note.CONTENT_MIME_TYPE, type);

		Uri url = ContentUris.withAppendedId(
				NotesProvider.Note.CONTENT_ITEM_URL, 1l);
		type = mResolver.getType(url);
		assertEquals(NotesProvider.Note.CONTENT_MIME_ITEM_TYPE, type);

	}

	public void testGetTypeUri_incorrect_uri() {
		String fakeUrl = "content://randomURL";
		try {
			mResolver.getType(Uri.parse(fakeUrl));
		} catch (IllegalArgumentException e) {

			assertEquals(true, e.getMessage().contains(fakeUrl));
		}

	}

}
