package com.personal.todo.test.dao;

import java.util.Calendar;
import java.util.List;

import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;
import android.test.mock.MockContext;

import com.personal.todo.dao.NotesDAOImpl;
import com.personal.todo.model.Note;

public class NotesDAOTest extends AndroidTestCase{

	private RenamingDelegatingContext context; 
	
	private static final String FILE_PREFIX="test_";
	
	
	public void setUp()
	{
		context = new RenamingDelegatingContext(new MockContext(),getContext(), FILE_PREFIX);
		
	}
	
	public void testcreateNote()
	{
		//NotesDAOImpl notesDao = new NotesDAOImpl(context);
		notesDao.openWritable();
		Note note = new Note();
		note.setDateCreated(Calendar.getInstance().getTime());
		note.setDateLastModified(Calendar.getInstance().getTime());
		note.setName("test");
		note.setDescription("test");
		
		Note createNote = notesDao.addNote(note);
		assertNotNull(createNote.getId());
		notesDao.close();	
	}
	
	public void testDeleteNode()
	{
		
		NotesDAOImpl notesDao = new NotesDAOImpl(context);
		notesDao.openWritable();
		
		Note note = new Note();
		note.setDateCreated(Calendar.getInstance().getTime());
		note.setDateLastModified(Calendar.getInstance().getTime());
		note.setName("test");
		note.setDescription("test");
		Note createNote = notesDao.addNote(note);
		boolean deleteNote = notesDao.removeNoteById(createNote.getId());
		assertTrue(deleteNote);
		List<Note> allNotes = notesDao.getAllNotes();
		assertNotNull(allNotes);
		assertEquals(0, allNotes.size());
		notesDao.close();
	}
	
	public void testGetAllNotes()
	{
		NotesDAOImpl notesDao = new NotesDAOImpl(context);
		notesDao.openWritable();
		
		Note note = new Note();
		note.setDateCreated(Calendar.getInstance().getTime());
		note.setDateLastModified(Calendar.getInstance().getTime());
		note.setName("test");
		note.setDescription("test");
		notesDao.addNote(note);
		List<Note> allNotes = notesDao.getAllNotes();
		assertNotNull(allNotes);
		assertEquals(1, allNotes.size());
		notesDao.close();
	}
}
