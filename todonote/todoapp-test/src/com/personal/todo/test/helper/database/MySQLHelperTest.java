package com.personal.todo.test.helper.database;

import java.util.Calendar;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;
import android.test.mock.MockContext;
import android.util.Log;

@TargetApi(16) public class MySQLHelperTest extends AndroidTestCase {

	private RenamingDelegatingContext context;

	@Override
	protected void setUp() throws Exception {
		context = new RenamingDelegatingContext(new MockContext(),getContext(), "test.");
		setContext(context);
		super.setUp();
	}
	public void testMysqlHelperMultiInstance()
	{
		
		MySQLHelper helper1 = new MySQLHelper(getContext());
		MySQLHelper helper2 = new MySQLHelper(context);
		SQLiteDatabase db1 = helper1.getWritableDatabase();

		SQLiteDatabase db2 = helper2.getWritableDatabase();
		insertData(db1);
		Cursor query = db2.query(true, MySQLHelper.TABLE_NOTES, null, null, null, null, null, null, null, null);
		assertEquals(1, query.getCount());
	}
	
	private void insertData(SQLiteDatabase testDB)
	{
		testDB.beginTransaction();
		ContentValues values = new ContentValues();
		
		values.put(MySQLHelper.COLUMN_NAME, "testName");
		values.put(MySQLHelper.COLUMN_DESCRIPTION, "testDesc");
		values.put(MySQLHelper.COLUMN_CREATION_DATE, Calendar.getInstance().getTime().toString());
		values.put(MySQLHelper.COLUMN_MODIFICATION_DATE,
				Calendar.getInstance().getTime().toString());
		long insertId = testDB.insert(MySQLHelper.TABLE_NOTES, null, values);
		testDB.setTransactionSuccessful();
		testDB.endTransaction();
		Log.i(NotesContentProvider.class.toString(), "data inserted:" + insertId);
		testDB.close();
	}
}
